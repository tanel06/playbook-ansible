# Création d'une stack LAMP dans Centos 8 via un Playbook Ansible

## Objectif

Ce Playbook Ansible nous procurera une solution alternative à l'installation manuelle d'un serveur LAMP (Linux, Apache, MySQL et PHP) sur Centos 8. L'exécution de ce Playbook va donc automatiser les opérations suivantes sur nos serveurs distants :

* Côté serveur web :
    ```
    * Installation des packages httpd, php et php-mysqlnd
    * Déploiement des codes sources de notre application 
    ```

* Côté serveur base de données :
    ```
    * Installation des packages mysql 
    * Autorisation du serveur web à communiquer avec la base de données
    * Configuration du table mysql avec les bons attributs et autorisations
    ```


## Indications et les étapes à suivre

1. **Recuperez ce repository dans votre machine :** ```git clone https://gitlab.com/tanel06/playbook-ansible.git```  
2. **Créez et provisionnez les machines virtuelles à l'aire de vagrant :**
    ```md
    * Déplacez-vous dans le repertoire vagrant de votre repository local et tapez la commande `vagrant up`
    * Si tout s'est bien passé, vous devriez avoir trois machines virtuelles installées dans votre Virtualbox (manager, worker1 et workers2).       L'Ansible a été installé sur la machine du manager. Vous pouvez le vérifier en tapant la commande `type ansible` sur la machine manager de votre virtualbox
    * Si vous souhaitez ouvrir une connexion ssh avec vos machines, exécutez la commande `vagrant ssh nom_de_la_machine`  
    ```
4. **Identifiez l'adresse IP de chaque machine virtuelle (les adresses IP ont été attribuées automatiquement par le serveur DHCP) et enregistrez-les dans un fichier car vous en aurez besoin plus tard.**
5. **Vérifiez la connectivité entre votre machine locale et les machines virtuelles (via ping )**
6. **Vérifiez la connectivité entre les machines virtuelles (via ping)**
7. **Mettez en place la connexion ssh entre  manager et les workers afin que nous puissions utiliser Ansible.**
8. **Modifiez le fichier d'inventaire hosts de votre Playbook**
    ```
    * éditez le fichier hosts se trouvant le répertoire principal du projet
    * remplacez l'adresse IP se trouvant dans le groupe [web] par l'adresse IP de votre worker1
    *  remplacez l'adresse IP se trouvant dans le groupe [db] par l'adresse IP de votre worker2
    ```
9. **Modifier le fichier des variables:**
    ```md
    * éditez le ficher main.yml se trouvant dans le repertoire `vars`
    * remplacez la valeur du variable db_host par l'adresse Ip du worker2 (`db_host: "adresse_ip_worker2"`)
    * remplacez la valeur du variable db_host par l'adresse Ip du worker2 (`webserver_host: "adresse_ip_worker2"`)
    ```
10. **Explication du fichier d'inventaire : **
    ```md
    * Par défaut, le fichier d'inventaire Ansible se trouve dans /etc/ansible/host. Mais dans ce projet, nous avons créé notre propre fichier d'inventaire nommé hosts. Pour que notre nouveau fichier d'inventaire soit pris en compte par notre Playbook, nous devons d'abord modifier la valeur de la variable `inventory` située dans le fichier de configuration ansible. Par défaut ansible utilise le fichier de configuration `/etc/ansible/ansible.cfg` mais on peut surcharger la config en rajoutant un fichier nommé `ansible.cfg` à la racine du projet.
    * Dans le fichier `ansible.cfg` de notre projet, nous précisons donc à ansible que nous utiliserons le fichier hosts que nous avons créé comme fichier d'inventaire par défaut
    ```

11. **Explication du Playbook:**
    ```md
    * Veuillez vous référer aux documentations que j'ai déjà écrites dans `playbook.yml` pour bien comprendre chaque tâche du playbook
    ```

12. **Maintenant, allez dans le répertoire principal du projet et tapez la commande:** ```ansible-playbook playbook.yml```
    ```
    * Cela permettra d'automatiser les tâches décrites précédemment dans la section objectif : 
        - sur le worker1 : Installation  d' apache et php + Deploiement du codes source sur le serveur web
        - Sur le worker2 : Installation de mysql + configurations nécessaires
    ```

13. **Testez si cela fonctionne:**
    ```md
    * sur l'url de votre navigateur, tapez : `http:\\adresse_ip_du_worker1`
    * Vous pouvez également consulter le contenu de votre base de données  si vous le souhaitez :
        - sur worker2, tapez pa commande : `mysql -u root -p '' `
        - après, tapez `use blog;`
        - Pour voir tous les articles, tapez : ` select * from articles`
    ````

    
