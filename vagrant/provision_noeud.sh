#!/bin/bash

# mise à jour de CentOS
sudo yum update -y
sudo yum install net-tools -y
# mise à jour du package manager de Fedora
sudo dnf makecache
sudo dnf install epel-release -y
# permet d'ajouter le serveur de fichiers (package) de docker
#sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf makecache
# télécharge la dernière version (option --no-best)
#sudo dnf install docker --nobest -y
# démarrage de docker
#sudo systemctl start docker
# démarrage du service docker en démon
#sudo systemctl enable docker
# curl permet de faire des requêtes HTTP (get/post/put/delete...)
sudo dnf install curl
# télécharge docker-compose et on le place dans /usr/local/bin/docker-compose
#sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
#sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
#sudo chmod +x /usr/bin/docker-compose

