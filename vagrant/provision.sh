#!/bin/bash
# update and installation 
sudo yum update -y
sudo dnf makecache

#deploquer le repo epel
sudo dnf install epel-release -y
sudo yum install curl -y #permet de faire des requete html (get/post/put/..)
sudo yum install vim -y
sudo yum install vi -y 
sudo yum install python3 python3-pip -y
sudo yum install net-tools -y
echo "update and installation of curl"

#ajout d'un utilisateur  admin
sudo adduser admin



#install ansible
sudo dnf makecache
sudo dnf install ansible -y

#utilitaire pour gerer les files system
sudo dnf install cifs-utils -y 


